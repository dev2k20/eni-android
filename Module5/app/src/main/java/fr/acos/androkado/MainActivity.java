package fr.acos.androkado;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import fr.acos.androkado.bo.Article;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "ACOS";
    Article article = null;

    public static final String CLE_TRI = "tri";
    public static final String CLE_DEFAULT = "default";
    public static final String NOM_FICHIER = "inter";


    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        //Charge le menu décrit du fichier action_bar_details.xml
        getMenuInflater().inflate(R.menu.action_bar_details,menu);
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        article = getIntent().getParcelableExtra("article");

        TextView tvNom =  findViewById(R.id.tv_article);
        TextView tvDescription = findViewById(R.id.tv_description);
        TextView tvPrix = findViewById(R.id.tv_prix);
        RatingBar rating = findViewById(R.id.rating_article);
        ToggleButton toggle = findViewById(R.id.btn_achete);

        tvNom.setText(article.getNom());
        tvDescription.setText(article.getDescription());
        tvPrix.setText(String.valueOf(article.getPrix()));
        rating.setRating(article.getNote());
        toggle.setChecked(article.isAchete());
    }

    public void onClickUrl(View view)
    {
        Log.i(TAG,"Lancement de l'activité InfoUrlActivity");
        Intent intent = new Intent(this, InfoUrlActivity.class);
        intent.putExtra("article",article);
        startActivity(intent);
    }

    public void onClickAchat(View view)
    {
        article.setAchete(!article.isAchete());
        Log.i(TAG,"Valeur achat " + article.isAchete());
    }
}
