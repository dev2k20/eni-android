package fr.acos.androkado;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import fr.acos.androkado.adapters.ArticleAdapter;
import fr.acos.androkado.bo.Article;

import static java.security.AccessController.getContext;

public class ListeArticlesActivity extends AppCompatActivity
{
    private static final String TAG = "ACOS";
    //Objet représentant le recyclerView
    private RecyclerView mRecyclerView;
    //Objet représentant l"adapter remplissant le recyclerView
    private RecyclerView.Adapter mAdapter;
    //Objet permettant de structurer l'ensemble des sous vues contenues dans le RecyclerView.
    private RecyclerView.LayoutManager mLayoutManager;
    //Liste bouchon
    private ArrayList<Article> articles = new ArrayList<>();

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        //Charge le menu décrit du fichier action_bar_liste.xml
        getMenuInflater().inflate(R.menu.action_bar_liste,menu);
        return true;
    }

    /**
     * Définition de l'action du clic sur un item.
     */
    private View.OnClickListener monClickListener = new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            int position = Integer.parseInt(view.getTag().toString());
            Log.i(TAG,"POSITION : " + view.getTag());
            Intent intent = new Intent(ListeArticlesActivity.this,MainActivity.class);
            intent.putExtra("article",articles.get(position));
            startActivity(intent);
        }
    };

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.item_configuration:
                Intent intent = new Intent(ListeArticlesActivity.this, ConfigurationActivity.class);
                startActivity(intent);
                //Toast.makeText(this, "Preferences", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.item_ajout:
                Toast.makeText(this, "Recherche", Toast.LENGTH_SHORT).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_liste_articles);

        mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);

        chargementBouchon();
        chargementRecycler();
    }

    /**
     * Permet de charger le recycler view
     */
    private void chargementRecycler()
    {
        //Lie le recyclerView aux fonctionnalité offerte par le linear layout
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        //Liaison permettant de structurer l'ensemble des sous vues contenues dans le RecyclerView.
        mAdapter = new ArticleAdapter(articles,monClickListener);
        mRecyclerView.setAdapter(mAdapter);
    }

    /**
     * Permet de charger le bouchon
     */
    private void chargementBouchon()
    {
        Article a1 = new Article(1,"Guitage","instrument de musique","http://www.guitare.fr",99.99f,4,false);
        Article a2 = new Article(2,"Flûte","instrument de musique","http://www.flute.fr",99.99f,4,false);
        Article a3 = new Article(3,"Basse","instrument de musique","http://www.basse.fr",99.99f,4,false);
        articles.add(a1);
        articles.add(a2);
        articles.add(a3);
    }
}
