package fr.acos.androkado;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Switch;
import android.widget.TextView;

import fr.acos.androkado.bo.Article;

public class ConfigurationActivity extends AppCompatActivity {

    private Switch tri_prix;
    private TextInputEditText default_prix;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configuration);


        tri_prix = findViewById(R.id.tri_prix);

        default_prix = findViewById(R.id.default_prix);

    }

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences sp = getSharedPreferences(MainActivity.NOM_FICHIER,MODE_PRIVATE);
        tri_prix.setChecked(sp.getBoolean(MainActivity.CLE_TRI, false));
        default_prix.setText(String.valueOf(sp.getFloat(MainActivity.CLE_DEFAULT, 0)));
    }

    public void onClickValidateConfig(View view) {
        SharedPreferences sp = getSharedPreferences(MainActivity.NOM_FICHIER,MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putBoolean(MainActivity.CLE_TRI, tri_prix.isChecked());
        editor.putFloat(MainActivity.CLE_DEFAULT, Float.parseFloat(default_prix.getText().toString().trim()));
        editor.apply();
        finish();
    }
}
