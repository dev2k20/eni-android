package fr.acos.androkado.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import fr.acos.androkado.R;
import fr.acos.androkado.bo.Article;


public class ArticleAdapter extends RecyclerView.Adapter<ArticleAdapter.ViewHolder> {
    //Permet de stocker les données à afficher.
    private ArrayList<Article> mDataset;
    private OnClicSurUnItem<Article> action;
    /**
     * Fournit une référence aux vues pour chaque élément de données
     */
    public class ViewHolder extends RecyclerView.ViewHolder implements
            View.OnClickListener
    {
        // Chaque élément contient seulement une TextView
        TextView mTextView;
        RatingBar mRatingBar;
        ViewHolder(View v) {
            super(v);
            mTextView = v.findViewById(R.id.article_info);
            mRatingBar = v.findViewById(R.id.rating_article);
            mRatingBar.setIsIndicator(true);
            v.setOnClickListener(this);
        }
        /**
         * Action réalisée lors d'un clic sur un élément du RecyclerView.
         */
        @Override
        public void onClick(View v) {
            action.onInteraction(mDataset.get(ViewHolder.this.getAdapterPosition()));
        }
    }
    /**
     * Interface devant être implémentée par l'activité définissant le
     RecyclerView
     */
    public interface OnClicSurUnItem<T> {
        void onInteraction(T info);
    }
    /**
     * Constructeur qui attend les données à afficher en paramètre
     **/
    public ArticleAdapter(List<Article> myDataset, OnClicSurUnItem<Article> activite) {
        mDataset = (ArrayList<Article>)myDataset;
        action = activite;
    }
    /**
     * Crée un ViewHolder qui représente le fichier my_text_view.xml
     **/
    @Override
    public ArticleAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int
            viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.my_text_view, parent, false);
        return new ViewHolder(v);
    }
    /**
     * Remplit la vue représentant une ligne
     */
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Article article = mDataset.get(position);
        holder.mTextView.setText(article.getNom());
        holder.mRatingBar.setRating(article.getNote());
    }
    /**
     * Retourne le nombre de données à afficher
     *
     * @return le nombre de données à afficher
     */
    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}
