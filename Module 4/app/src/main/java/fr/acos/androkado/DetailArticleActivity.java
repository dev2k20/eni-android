package fr.acos.androkado;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import fr.acos.androkado.bo.Article;

public class DetailArticleActivity extends AppCompatActivity {

    private static final String TAG = "ACOS";
    Article article = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_article);

        Toolbar myToolBar = (Toolbar) findViewById(R.id.toolbar_details);
        setSupportActionBar(myToolBar);

        Intent intent = getIntent();
        article = intent.getParcelableExtra("article");
        //article = new Article();
        //article.setId(1);
        //article.setNom("Pain au chocolat");
        //article.setDescription("Viennoiserie au beurre et au chocolat.");
        //article.setUrl("http://www.painauchocolat.fr");
        //article.setPrix(0.95F);
        //article.setNote(3);
        //article.setAchete(true);

        TextView tvNom =  findViewById(R.id.tv_article);
        TextView tvDescription = findViewById(R.id.tv_description);
        TextView tvPrix = findViewById(R.id.tv_prix);
        RatingBar rating = findViewById(R.id.rating_article);
        ToggleButton toggle = findViewById(R.id.btn_achete);

        tvNom.setText(article.getNom());
        tvDescription.setText(article.getDescription());
        tvPrix.setText(String.valueOf(article.getPrix()));
        rating.setRating(article.getNote());
        toggle.setChecked(article.isAchete());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_editer:
                Toast.makeText(this, "Editer", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.action_envoyer:
                Toast.makeText(this, "Envoyer", Toast.LENGTH_SHORT).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }



    public void onClickUrl(View view)
    {
        Log.i(TAG,"Lancement de l'activité InfoUrlActivity");
        Intent intent = new Intent(this, InfoUrlActivity.class);
        intent.putExtra("article",article);
        startActivity(intent);
    }

    public void onClickAchat(View view)
    {
        article.setAchete(!article.isAchete());
        Log.i(TAG,"Valeur achat " + article.isAchete());
    }
}
