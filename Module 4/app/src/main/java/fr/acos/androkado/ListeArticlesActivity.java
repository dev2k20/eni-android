package fr.acos.androkado;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import fr.acos.androkado.adapters.ArticleAdapter;
import fr.acos.androkado.bo.Article;

public class ListeArticlesActivity extends AppCompatActivity implements ArticleAdapter.OnClicSurUnItem<Article> {
    private static final String TAG = "ListeArticlesActivity";
    public List<Article> listeArticles = new ArrayList<Article>();

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_liste_articles);

        Toolbar myToolBar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolBar);


        Article art1 = new Article(0, "Pain au chocolat", "Un pain de qualité", "pain-au-chocolat.fr", 10, 5, true);
        Article art2 = new Article(0, "Grenadine", "Sirop de pêche", "grenadine.fr", 2, 4, true);
        Article art3 = new Article(0, "Cacahuète", "Cachuète", "cacahuete.fr", 1, (float) 3.5, true);
        this.listeArticles.add(art1);
        this.listeArticles.add(art2);
        this.listeArticles.add(art3);


        mRecyclerView = findViewById(R.id.recycler_view);

        // Permet d’optimiser le chargement dans le cas
        //où le RecyclerView ne change pas de taille
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        String[] infos = {"Bleu","Blanc","Rouge"};
        //Lie l’adapter au RecyclerView
        mAdapter = new ArticleAdapter(this.listeArticles, this);
        mRecyclerView.setAdapter(mAdapter);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                Toast.makeText(this, "Preferences", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.action_ajouter:
                Toast.makeText(this, "Recherche", Toast.LENGTH_SHORT).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public void onInteraction(Article article) {
        Log.i(TAG,"Lancement de l'activité DetailArticleActivity");
        Intent intent = new Intent(this, DetailArticleActivity.class);
        intent.putExtra("article", article);
        startActivity(intent);
    }
}
