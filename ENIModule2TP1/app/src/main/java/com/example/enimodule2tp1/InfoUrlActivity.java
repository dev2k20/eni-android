package com.example.enimodule2tp1;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.enimodule2tp1.bo.Article;

public class InfoUrlActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_url);

        TextView txtUrl = (TextView) findViewById(R.id.txt_url);

        Article article = (Article) getIntent().getSerializableExtra("article");
        assert article != null;
        txtUrl.setText(article.getUrl());

    }

}
