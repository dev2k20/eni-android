package com.example.enimodule2tp1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.example.enimodule2tp1.bo.Article;

public class MainActivity extends AppCompatActivity {

    private Article article;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        article = new Article();
        article.setLibelle("Pain au chocolat");
        article.setPrice(10);
        article.setDescription("Ceci est un croissant");
        article.setRating(2);
        article.setAchete(false);
        article.setUrl("eni-training.fr");

        TextView txtLibelle = (TextView) findViewById(R.id.item);
        TextView txtPrix = (TextView) findViewById(R.id.price);
        TextView txtDescription = (TextView) findViewById(R.id.description);
        RatingBar ratingBar = (RatingBar) findViewById(R.id.rating);
        ToggleButton btnAchete = (ToggleButton) findViewById(R.id.btn2);

        txtLibelle.setText(article.getLibelle());
        txtPrix.setText(String.valueOf(article.getPrice()));
        txtDescription.setText(article.getDescription());
        ratingBar.setRating(article.getRating());
        btnAchete.setChecked(article.isAchete());
    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    public void onBuyClick(View view) {
        this.article.setAchete(!this.article.isAchete());
    }

    public void onPlanetClick(View view) {
        //Toast.makeText(this, this.article.getUrl(), Toast.LENGTH_LONG).show();
        Intent infoUrlActivity = new Intent(this, InfoUrlActivity.class);
        infoUrlActivity.putExtra("article", this.article);
        this.startActivity(infoUrlActivity);
    }


}