package com.example.enimodule2tp1.bo;

import java.io.Serializable;

public class Article implements Serializable {
    private String libelle;
    private String url;
    private float price;
    private String description;
    private int rating;
    private boolean isAchete;

    public Article() {

    }

    public Article(String libelle, String url, float price, String description, int rating, boolean isAchete) {
        this.libelle = libelle;
        this.url = url;
        this.price = price;
        this.description = description;
        this.rating = rating;
        this.isAchete = isAchete;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public boolean isAchete() {
        return isAchete;
    }

    public void setAchete(boolean achete) {
        isAchete = achete;
    }
}
